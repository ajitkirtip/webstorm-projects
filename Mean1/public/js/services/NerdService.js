/**
 * Created by ajit on 7/8/15.
 */
angular.module('NerdService', []).factory('Nerd', ['$http', function($http){
    return {
        // call to get all nerds
        get:function(){
            return $http.get('/api/nerds');
        },

        // these will work when more api routes are defined in the node
        // call to post and create a new nerd
        create: function(nerdData){
            return $http.post('/api/nerds', nerdData);
        },

        // call to delete a nerd
        delete: function(id){
            return $http.delete('/api/nerds' + id);
        }
    }
}]);