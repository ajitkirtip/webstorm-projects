/**
 * Created by ajit on 7/8/15.
 */
angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
    $routeProvider
    // home page
        .when('/', {
            templateUrl : 'views/home.html',
            controller : 'MainController'
        })

     //nerds page that will use the Nerd Controller
        .when('/nerds', {
            templateUrl: 'views/nerd.html',
            controller: 'NerdController'
        });

    //avoiding page refresh
    $locationProvider.html5Mode(true);
}]);